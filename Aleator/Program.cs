﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aleator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Numarul de linii este: ");
            int m = int.Parse(Console.ReadLine());

            Console.WriteLine("Numarul de coloane este: ");
            int n = int.Parse(Console.ReadLine());

            Random rand = new Random();

            int[,] matrice = new int[m, n];
            int[] VFrecv = new int[11];

            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                {
                    matrice[i, j] = rand.Next(1, 11);
                    VFrecv[matrice[i, j]]++;
                }

            int SumDgPrinc= 0, SumDgInv = 0;

            if (m <= n)
            {
                for (int i = 0; i < m; i++)
                {
                    SumDgPrinc += matrice[i, i];
                    SumDgInv += matrice[i, n - i - 1];
                    
                }

            }
            else
            {
                for (int i = 0; i < n; i++)
                {
                    SumDgPrinc += matrice[i, i];
                    SumDgInv += matrice[i, n - i - 1];
                }
            }
         
             
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write(matrice[i, j] + " ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Suma de pe diagonala principala este " + SumDgPrinc + " iar suma de pe diagonala secundara este " + SumDgInv);

            for (int i = 1; i <= 10; i++)
                Console.WriteLine("Numarul " + i + " se repeta in matrice de " + VFrecv[i] + " ori.");

            Console.ReadKey();

        }
    }
}
